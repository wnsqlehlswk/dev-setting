# docker in ubuntu 19.04 in termux in andriod device

check cpu architecture before install
lsb_release -a

## galaxy A30

device spec link https://www.gsmarena.com/samsung_galaxy_a30-9579.php

### lsb_release -a

Distributor ID: Ubuntu
Description: Ubuntu 19.04
Release: 19.04
Codename: disco

### uname -a

Linux localhost 4.4.111-16470226 #1 SMP PREEMPT Fri Jul 26 09:50:51 KST 2019 aarch64 Android

-   0. install termux

https://wiki.termux.com/wiki/Installation

-   1. install openssh

```sh
pkg install open ssh
sshd
```

-   2. check id for ssh connect

```sh
whoami
```

-   3. check ip address

```sh
ifconfig
```

-   4. change password for safty

```sh
passwd
```

-   5. install ubuntu in termux and start

https://github.com/MFDGaming/ubuntu-in-termux

-   6. install ssh in ubuntu

https://zetawiki.com/wiki/%EC%9A%B0%EB%B6%84%ED%88%AC_sshd_%EC%84%A4%EC%B9%98

-   7.install docker in ubuntu

```sh
apt-get update
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y
apt-get install docker.io
```
